/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.latihan04d;
import java.util.Scanner;
/**
 *
 * @author Rafi
 */
public class Latihan04d {

    public static void main(String[] args) {
        Scanner inp= new Scanner(System.in);
        TagihanAir ta = new TagihanAir();
        System.out.println("Perhitungan Biaya Pemakaian Air");
        System.out.println("==============================");
        System.out.print("Nama \t\t: ");
        String nama = inp.nextLine();
        ta.setNama(nama);
        System.out.print("No. Pelanggan \t: ");
        String np = inp.nextLine();
        ta.setNama(np);
        System.out.print("Pemakaian Air \t: ");
        int pakai = inp.nextInt();
        ta.setPakai(pakai);
        ta.hitung();
        System.out.println("Biaya Pakai \t: " + ta.total);
        System.out.println("=============================");
    }
}
