/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package impllementasi_smartphone;
import java.lang.Math;
import java.util.HashMap;
import java.util.Map;
/**
 *
 * @author Administrator
 */
public class Cellphone implements Smartphone {
    String merk;
    String type;
    int batteryLevel;
    int status;
    int volume;
    int credit;
    
    HashMap<String, String> kontak = new HashMap<>();

    public Cellphone(String merk,String type)
    {
        this.merk = merk;
        this.type = type;
        this.batteryLevel = (int)(Math.random()*(100-0+1)+0);  
    }

    public void powerOn(){
        this.status = 1;
        System.out.println("Ponsel menyala");
    }
    
    public void powerOff(){
        this.status = 0;
        System.out.println("Ponsel mati");
    }

    public void volumeUp(){
        if(this.status == 0){
        System.out.println("Ponsel mati. Tidak dapat menaikkan volume");
        }
        else
        {
            this.volume++;
            if(this.volume>=100)
            {
                this.volume = 100;
            }
        }
    }

    public void volumeDown(){
        if(this.status == 0){
        System.out.println("Ponsel mati. Tidak dapat menaikkan volume");
        }
        else
        {
            this.volume--;
            if(this.volume<=0)
            {
                this.volume = 0;
            }
        }
    }

    public int getVolume(){
        return this.volume;
    }

    @Override
    public void topUpCredit(int credit) {
        if(this.status == 0){
        System.out.println("Ponsel mati. Tidak dapat menambah pulsa");
        }
        else
        {
            this.credit += credit;
            
            System.out.println("Pulsa bertambah " + credit + ". Sisa pulsa:" + this.credit);
        }
    }

    @Override
    public void checkCreditBalance() {
        if(this.status == 0){
        System.out.println("Ponsel mati. Tidak dapat menambah pulsa");
        }
        else
        {
            this.credit += credit;
            
            System.out.println("Pulsa bertambah " + credit + ". Sisa pulsa:" + this.credit);
        }
    }

    @Override
    public void addContact(String no, String nama) {
        if(this.status == 0){
            System.out.println("Ponsel mati. Tidak dapat menambah kontak");
        }
        else
        {
            kontak.put(no, nama);
            System.out.println("Kontak dengan nama: " + nama + " dan nomor: " + no + " telah disimpan");
        }
    }

    @Override
    public void viewAllContacts() {
        if(this.status == 0){
            System.out.println("Ponsel mati. Tidak dapat melihat kontak");
        }
        else
        {
            System.out.println("Daftar Kontak: ");
            for (Map.Entry<String, String> e : kontak.entrySet()) {
                System.out.println("Nama: " + e.getValue());
                System.out.println("Nomor: " + e.getKey() + "\n");
            }
        }
    }

    @Override
    public void searchContacts(String nama) {
        if(this.status == 0){
            System.out.println("Ponsel mati. Tidak dapat mencari kontak");
        }
        else
        {
            for (Map.Entry<String, String> e : kontak.entrySet()) {
                if(e.getValue() == nama) {
                    System.out.println("Pencarian kontak Harry ketemu");
                    System.out.println("Nama: " + e.getValue());
                    System.out.println("Nomor: " + e.getKey());
                }
            }
        }
    }
}
