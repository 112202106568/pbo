/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package impllementasi_smartphone;

/**
 *
 * @author Administrator
 */
public class CellphoneMain {
    public static void main(String[] args){
        Cellphone cp = new Cellphone("Infinix", "Hot 9 play");
        
        cp.powerOn();
        
        cp.topUpCredit(10000);
        cp.addContact("078123545", "Harry");
        cp.addContact("081234567890", "Diana");
        
        cp.viewAllContacts();
        
        cp.searchContacts("Harry");
        
        cp.powerOff();
    }    
}
