/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.latihan04c;
import java.util.Scanner;
/**
 *
 * @author Rafi
 */
public class Latihan04c {

    public static void main(String[] args) {
        Scanner inp= new Scanner(System.in);
        NilaiMhs nilai = new NilaiMhs();
        System.out.println("Data Test");
        System.out.println("==============================");
        System.out.print("Nama \t\t: ");
        String nama = inp.nextLine();
        nilai.setNama(nama);
        System.out.print("Program Studi \t: ");
        String prodi = inp.nextLine();
        nilai.setProdi(prodi);
        System.out.print("Nilai \t\t: ");
        int aNilai = inp.nextInt();
        nilai.setNilai(aNilai);
        nilai.hasil();
        System.out.println("Nilai huruf \t: " + nilai.kNilai);
        System.out.println("=============================");
    }
}
