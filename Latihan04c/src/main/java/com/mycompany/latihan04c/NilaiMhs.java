/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan04c;

/**
 *
 * @author Rafi
 */
public class NilaiMhs
{
    String nama;
    String prodi;
    int nilai;
    char kNilai;
    void setNama(String inNama)
    {
        nama= inNama;
        
    }
    void setProdi(String inProdi)
    {
        prodi= inProdi;
    }
    void setNilai(int inNilai)
    {
        nilai= inNilai;
    }
    void hasil()
    {
       if(nilai <= 100 && nilai >= 85){
           kNilai =  'A';
        } else if(nilai <= 85 && nilai >=75){
            kNilai = 'B';
        } else if(nilai <=75 && nilai >= 60){
            kNilai = 'C';
        } else if(nilai <=60 && nilai >= 50){
            kNilai = 'D';
        } else if(nilai <=50 && nilai >= 0){
            kNilai = 'E';
        }
    }
}
